import { Component } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { TimeWorked } from './models/timeWorked.model';

const counter = timer(0, 1000);
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  ngOnInit() {
  }
  title = 'Geraki';
  toggle: boolean = false;
  display: string = '';
  interval: any;
  timeWorked: TimeWorked = {}
  clock: string = '';
  time(event: any) {
    this.toggleFlag();
    let date: Date = new Date();
    if (this.toggle === true) {
      this.timeWorked.Start = new Date();
    }
    if (this.toggle === false) {
      this.timeWorked.End = new Date();
      let diff = toTime(this.timeWorked.End as Date) -
        toTime(this.timeWorked.Start as Date);
      this.timeWorked.TimePassed = getTimeWorked(this.timeWorked.Start as Date, this.timeWorked.End as Date);
      console.log('time', getTimeWorked(this.timeWorked.Start as Date, this.timeWorked.End as Date));
    }

    let second: number | string = date.getSeconds();
    let minute: number | string = date.getMinutes();
    let hour: number | string = date.getHours();
    if (second < 10) {
      second = '0' + second
    }
    if (minute < 0) {
      minute = '0' + minute;
    }
    this.clock = hour + ":" + minute + ":" + second;
  }
  toggleFlag() {
    this.toggle = !this.toggle;
  }

}
function toTime(time: Date) {
  return time.getTime()
}
function getTimeWorked(start: Date, end: Date) {
  let timeDifference = end.getTime() - start.getTime();
  return new Date(Math.floor(timeDifference / 1000 % 60) * 1000).toISOString().substr(11, 8)
}