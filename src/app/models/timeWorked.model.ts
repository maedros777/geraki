export interface TimeWorked {
    Start?: Date;
    End?: Date;
    TimePassed?: string;
}