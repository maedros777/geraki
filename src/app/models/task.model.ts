export interface Task {
    id: String;
    Title: String;
    Description: String;
}